config_dict = {
    "date": "2021-04-09",
    "orbitID": 1001,
    "lat_bounds": [25, 50],
    "lon_bounds": [-130, 65],
    "input_directory": "/data/input",
    "output_directory": "/data/output"
}
#print(config_dict)

# Change the latitude bounds to cover the full Northern Hemisphere
config_dict["lat_bounds"] = [0, 90]
#print(config_dict)

# Access an item in the dictionary that doesn't exist using .get notation and return -999
print(config_dict.get("DNE"))


# Using a loop, add one to the orbitID value in the dictionary
for k, v in config_dict.items():
    if "orbitID" in k:
        config_dict[k]+=1

print(config_dict)

#print(config_dict["orbitID"])

# Access an item in the dictionary that doesn't exist using bracket notation and handle the exception
key = "DNE"
try:
    print(config_dict[key])
except KeyError:
    print("Key " + key + " doesn't exist")
    print("Available keys:")
    print(config_dict.keys())
    
# (Bonus) Reorganize the dictionary to group information by date and add information for a second date
new_config_dict = {
    "2021-04-09": {
        "orbitID": 1001,
        "lat_bounds": [25, 50],
        "lon_bounds": [-130, 65],
        "input_directory": "/data/input",
        "output_directory": "/data/output"},
    "2021-04-10": {
        "orbitID": 2001,
        "lat_bounds": [25, 50],
        "lon_bounds": [-130, 65],
        "input_directory": "/data/input",
        "output_directory": "/data/output"}
}

#print(new_config_dict.keys())
#print()
#print(new_config_dict)
