# Define dictionary
config_dict = {
    "date": "2021-04-09",
    "orbitID": 1001,
    "lat_bounds": [25, 50],
    "lon_bounds": [-130, 65],
    "input_directory": "/data/input",
    "output_directory": "/data/output"
}

# Add/modify items
config_dict["region"] = "CONUS"                 #key did not exist
config_dict["output_directory"] = "/data/plots" #key existed already

# Remove items
config_dict.pop("region")
#config_dict.pop("DNE")


# Iterate over keys
for k in config_dict:
    print(k)
for k in config_dict.keys():
    print(k)

# Iterate over Values
for v in config_dict.values():
    print(v)

# Iterate over Items
for k,v in config_dict.items():
    print(k)
    print(v)